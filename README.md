# GitLab Security Team

This repository is for tracking security team issues and initiatives.

For security issues, please refer to ~security labeled issues in the following
projects:

 * [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security)
 * [gitlab-ee](https://gitlab.com/gitlab-org/gitlab-ee/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security)
 * [production other infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security).

